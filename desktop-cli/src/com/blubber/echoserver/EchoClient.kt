package com.blubber.echoserver

import com.blubber.sockets.JvmClientSocket
import com.blubber.sockets.inetSocketAddress
import kmulti.io.use
import kmulti.logging.topLevelLogger
import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.runBlocking
import kotlinx.serialization.toUtf8Bytes
import java.net.InetSocketAddress
import java.nio.channels.AsynchronousSocketChannel

const val PORT = 12345
const val CLIENT_READ_TIMEOUT = 1000L // 1 sec
const val CLIENT_WRITE_TIMEOUT = 3000L // 3 sec

private val logger = topLevelLogger {}

fun main(args: Array<String>) = runBlocking {
    logger.debug { "Connecting to server on port $PORT" }

    val remoteAddress = inetSocketAddress(port = PORT)
    val clientSocket = JvmClientSocket(remoteAddress)
    clientSocket.connect().use { socket ->
        logger.debug { "Connected to remoteAddress $remoteAddress" }
        while (true) {
            val toSendDeferred = async {
                readLine()
            }
            val toSend = toSendDeferred.await() ?: continue
            val bytesWritten = socket.write(toSend.toUtf8Bytes(), timeout = CLIENT_WRITE_TIMEOUT)
            if (bytesWritten < 0) {
                logger.debug { "Could not write any bytes, assuming socket was closed" }
                break
            }
            logger.debug { "Attempting socket read" }
            val bytesRead = socket.read(timeout = CLIENT_READ_TIMEOUT)
            if (bytesRead == null) {
                logger.debug { "Could not read any bytes, assuming socket was closed" }
                break
            }
            val echo = String(bytesRead)
            logger.debug { "Got echo: $echo" }
            if (echo == "stop") {
                logger.debug { "Got stop signal, stopping EchoClient." }
                break
            }
        }
    }
}
