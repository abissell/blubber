package com.blubber.terminal

import com.blubber.data.config.ExchangeSpec
import com.blubber.data.exchange.Exch
import com.blubber.event.ConfigEvent
import com.blubber.event.EventHead
import com.blubber.event.EventOrigin
import kmulti.io.use
import kmulti.logging.CompanionLogger
import kmulti.test.runTest
import kotlinx.coroutines.experimental.Job
import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.launch
import kotlinx.io.ByteBuffer
import kotlin.test.Test
import kotlin.test.assertTrue

class InTerminalBaseTest {
    companion object : CompanionLogger()

    @Test
    fun testInTerminalBase() = runTest {
        val configEvent = ConfigEvent(
            head = EventHead(
                seqNum = 1L,
                tsMillis = 2L,
                tsNanos = 3L,
                origin = EventOrigin.UI
            ),
            config = ExchangeSpec(exch = Exch.POLONIEX)
        )
        val byteBufferMaker = suspend {
            ByteBuffer.allocate(4)
        }
        val parent = Job()
        var runCount = 0
        val uiTerminal = InTerminalBase(
            byteBufferMaker,
            { _ -> configEvent },
            { event ->
                logger.debug { "event: $event" }
                runCount++
                parent.cancel()
            }
        )

        uiTerminal.open(parent = parent).use {
            val job = launch {
                byteBufferMaker()
            }
            delay(200)
            job.cancel()
        }

        assertTrue { runCount == 1 }
    }
}
