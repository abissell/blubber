package com.blubber.message

import com.blubber.data.config.*
import com.blubber.data.exchange.Exch
import kmulti.logging.CompanionLogger
import kotlinx.serialization.*
import kotlinx.serialization.internal.IntSerializer
import kotlinx.serialization.internal.StringSerializer
import kotlinx.serialization.json.JSON
import kotlin.math.roundToLong
import kotlin.test.Test
import kotlin.test.assertTrue

class JsonSerializationTest {
    companion object : CompanionLogger()

    @Test
    fun testJsonSerialization() {
        val testClass = mapOf("blah" to "blah")
        val stringified = JSON.stringify(
            (StringSerializer to StringSerializer).map,
            testClass
        )
        logger.debug { stringified }

        val exchangeSpec = ExchangeSpec(
            exch = Exch.POLONIEX,
            apiKey = "apiKey",
            secretKey = "secretKey",
            port = 80,
            shouldLoadRemoteMetaData = true
        )
        val serialized = JSON.stringify(exchangeSpec)
        logger.debug { serialized }
        val deserialized = JSON.parse<ExchangeSpec>(serialized)
        logger.debug { deserialized }

        val exchangeSpecSerializer: KSerializer<ExchangeSpec> = ExchangeSpec.serializer()
        val exchangeSpecSetSerializer = exchangeSpecSerializer.set
        val exchangeSpecSet = setOf(exchangeSpec)
        logger.debug { JSON.stringify(exchangeSpecSetSerializer, setOf(exchangeSpec)) }

        logger.debug { 0.0 % 1 }
        logger.debug { 0.1 % 1 }
        logger.debug { 2.0 % 1 }
        logger.debug { 2.1 % 1 }

        assertTrue { (2.0.roundToLong().toDouble()) == 2.0 }

        val constructedConfig = mapOf("exchSpecs" to exchangeSpecSet)
        val constructedConfigSerializer =
            (StringSerializer to exchangeSpecSetSerializer).map
        val constructedSerializedConfig =
            JSON.stringify(
                constructedConfigSerializer,
                constructedConfig
            )
        logger.debug { constructedSerializedConfig }
        logger.debug { JSON.parse(constructedConfigSerializer, constructedSerializedConfig) }

        val list = listOf(1, 2, 3, 4)
        val listJson = JSON.stringify(IntSerializer.list, list)
        logger.debug { listJson }
        val deserList = JSON.parse(IntSerializer.list, listJson)
        logger.debug { deserList }

        val set = setOf(1, 2, 3, 4)
        val setJson = JSON.stringify(IntSerializer.set, set)
        logger.debug { setJson }
        val deserSet = JSON.parse(IntSerializer.set, setJson)
        logger.debug { deserSet }
    }
}

