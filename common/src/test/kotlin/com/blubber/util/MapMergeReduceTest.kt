package com.blubber.util

import kotlin.test.Test
import kotlin.test.assertTrue

class MapMergeReduceTest {
    @Test
    fun testMapMergeReduce() {
        val map = mapOf("one" to 1, "two" to 0)
        val newMap = map.mergeReduce(mapOf("two" to 1)) { a, b ->
            if (b == 2) b else a
        }
        assertTrue { newMap["one"] == 1 }
        assertTrue { newMap["two"] == 0 }
        val newMap2 = map.mergeReduce(mapOf("two" to 2)) { a, b ->
            if (b == 2) b else a
        }
        assertTrue { newMap2["one"] == 1 }
        assertTrue { newMap2["two"] == 2 }
    }
}
