package com.blubber.data.config

import com.blubber.data.exchange.Exch
import kotlin.test.Test
import kotlin.test.assertTrue

class ConfigTest {
    @Test
    fun testConfig() {
        val exchSpec1 = ExchangeSpec(
                exch = Exch.POLONIEX,
                apiKey = "apiKey",
                secretKey = "secretKey"
        )
        val config = Config.EMPTY.newConfig(exchSpec1)

        val exchSpec2 = ExchangeSpec(
                exch = Exch.POLONIEX,
                apiKey = "apiKey2",
                secretKey = "secretKey2"
        )
        val newConfig = config.newConfig(exchSpec2)

        assertTrue { newConfig.exchSpecs == mapOf(1 to exchSpec1, 2 to exchSpec2) }
    }
}
