package com.blubber.data.currency

import kotlin.test.Test
import kotlin.test.assertTrue

class CurrencyPairTest {
    @Test
    fun testCurrencyPairCreation() {
        val pair = CurrencyPair(Currency("XMR"), Currency("BTC"))
        assertTrue { pair.base == Currency("XMR") }
        assertTrue { pair.counter == Currency("BTC") }
    }
}
