package com.blubber.data.order

import com.blubber.data.currency.Currency
import com.blubber.data.currency.CurrencyPair
import kmulti.bignumber.BigDecimal
import kmulti.bignumber.ONE
import kmulti.bignumber.ZERO
import kotlin.test.Test
import kotlin.test.assertTrue

class LimitOrderTest {
    @Test
    fun testLimitPriceCompare() {
        val higherBid = createLimitOrder(OrderType.BID, BigDecimal("5000.00"))
        val lowerBid = createLimitOrder(OrderType.BID, BigDecimal("4998.00"))
        assertTrue { higherBid < lowerBid }
        val lowerAsk = createLimitOrder(OrderType.ASK, BigDecimal("4990.00"))
        val higherAsk = createLimitOrder(OrderType.ASK, BigDecimal("4991.00"))
        assertTrue { lowerAsk < higherAsk }
        assertTrue { higherBid < lowerAsk }
    }

    private fun createLimitOrder(type: OrderType, limitPrice: BigDecimal) =
        LimitOrder(
            type,
            OrderStatus.NEW,
            ONE,
            ZERO,
            BigDecimal("0.5"),
            CurrencyPair(Currency("XMR"), Currency("BTC")),
            null,
            null,
            limitPrice,
            emptySet()
        )
}
