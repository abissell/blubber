package com.blubber.terminal

import kmulti.io.Closeable
import kotlinx.coroutines.experimental.DefaultDispatcher
import kotlinx.coroutines.experimental.Job
import kotlin.coroutines.experimental.CoroutineContext

interface InTerminal<T> : Closeable {
    fun open(
        ctxt: CoroutineContext = DefaultDispatcher,
        parent: Job? = null
    ): InTerminal<T>

    suspend fun read(): T

    suspend fun ingest(t: T)
}
