package com.blubber.terminal

import kmulti.logging.CompanionLogger
import kotlinx.coroutines.experimental.Job
import kotlinx.coroutines.experimental.launch
import kotlinx.io.ByteBuffer
import kotlin.coroutines.experimental.CoroutineContext
import kotlin.jvm.Volatile

class InTerminalBase<T>(
    private val receive: suspend () -> ByteBuffer,
    private val deserialize: (ByteBuffer) -> T,
    private val send: suspend (T) -> Unit
) : InTerminal<T> {

    companion object : CompanionLogger()

    @Volatile
    private var job: Job? = null

    override fun open(ctxt: CoroutineContext, parent: Job?): InTerminalBase<T> {
        val newJob = launch(context = ctxt, parent = parent) {
            while (isActive) {
                try {
                    val item = read()
                    ingest(item)
                } catch (ex: Throwable) {
                    logger.error { "Failed run of event loop because of $ex" }
                }
            }
        }

        job = newJob
        return this
    }

    override suspend fun read(): T {
        val rawEvent = receive()
        return deserialize(rawEvent)
    }

    override suspend fun ingest(t: T) {
        send(t)
    }

    override fun close() {
        job?.cancel()
    }
}
