package com.blubber.data.currency

expect class Currency private constructor(
        commonCode: String,
        isoCode: String?,
        name: String?,
        symbol: String?,
        alternativeCodes: List<String>?) : Comparable<Currency> {

    companion object {
        operator fun invoke(code: String): Currency
    }

    val commonCode: String
    val isoCode: String?
    val name: String?
    val symbol: String?
    val alternativeCodes: List<String>?
}

fun Currency.compare(other: Currency): Int {
    var comparison = commonCode.compareTo(other.commonCode)

    if (comparison == 0) {
        comparison = if (name == null) {
            if (other.name == null) 0 else 1
        } else {
            if (other.name == null) -1 else name.compareTo(other.name)
        }
    }

    if (comparison == 0) {
        comparison = hashCode() - other.hashCode()
    }

    return comparison
}
