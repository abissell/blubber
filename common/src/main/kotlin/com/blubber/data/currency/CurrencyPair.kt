package com.blubber.data.currency

expect class CurrencyPair(
        base: Currency,
        counter: Currency) : Comparable<CurrencyPair> {
    val base: Currency
    val counter: Currency
}

fun CurrencyPair.compare(o: CurrencyPair): Int {
    return (base.compareTo(o.base) shl 16) + counter.compareTo(o.counter)
}
