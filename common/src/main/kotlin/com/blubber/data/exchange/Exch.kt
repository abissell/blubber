package com.blubber.data.exchange

enum class Exch {
    BINANCE,
    BITFINEX,
    BITFLYER,
    BITMEX,
    BITSTAMP,
    BITTREX,
    COINMATE,
    CRYPTOPIA,
    OKCOIN,
    POLONIEX,
    GDAX,
    GEMINI,
    HITBTC,
    KRAKEN,
    WEX
}
