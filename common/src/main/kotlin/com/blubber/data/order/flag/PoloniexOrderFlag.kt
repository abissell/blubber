package com.blubber.data.order.flag

import com.blubber.data.order.OrderFlag

enum class PoloniexOrderFlag : OrderFlag {
    FILL_OR_KILL,
    IMMEDIATE_OR_CANCEL,
    POST_ONLY,
    MARGIN
}
