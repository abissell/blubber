package com.blubber.data.order

import com.blubber.data.currency.CurrencyPair
import kmulti.bignumber.BigDecimal

expect class LimitOrder(
    type: OrderType,
    status: OrderStatus?,
    originalAmount: BigDecimal,
    cumulativeAmount: BigDecimal?,
    averagePrice: BigDecimal?,
    currencyPair: CurrencyPair,
    id: String?,
    ts: Long?,
    limitPrice: BigDecimal,
    orderFlags: Set<OrderFlag>
) : Comparable<LimitOrder> {
    val type: OrderType
    val status: OrderStatus?
    val originalAmount: BigDecimal
    val cumulativeAmount: BigDecimal?
    val averagePrice: BigDecimal?
    val currencyPair: CurrencyPair
    val id: String?
    val ts: Long?
    val limitPrice: BigDecimal
    val orderFlags: Set<OrderFlag>
}

fun LimitOrder.compare(other: LimitOrder): Int {
    return if (type == other.type) {
        // Same side
        limitPrice.compareTo(other.limitPrice) *
                (if (type == OrderType.BID) -1 else 1)
    } else {
        // Keep bid side be less than ask side
        if (type == OrderType.BID) -1 else 1
    }
}
