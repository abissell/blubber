package com.blubber.data.order

data class OrderGuide(val placeholder: Unit) {
    companion object {
        val EMPTY = OrderGuide(Unit)
    }
}
