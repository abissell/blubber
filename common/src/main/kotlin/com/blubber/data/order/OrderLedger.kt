package com.blubber.data.order

import com.blubber.data.currency.CurrencyPair
import com.blubber.data.exchange.Exch

data class OrderLedger(
        val map: Map<Exch, Map<CurrencyPair, List<LimitOrder>>>) {
    companion object {
        val EMPTY = OrderLedger(emptyMap())
    }
}
