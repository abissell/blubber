package com.blubber.data.order

enum class OrderType {
    /**
     * Buying order (the trader is providing the counter currency)
     */
    BID,
    /**
     * Selling order (the trader is providing the base currency)
     */
    ASK
}
