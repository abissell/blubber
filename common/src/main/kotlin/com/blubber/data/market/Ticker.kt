package com.blubber.data.market

import com.blubber.data.currency.CurrencyPair
import kmulti.bignumber.BigDecimal

expect class Ticker : MarketData {
    val currencyPair: CurrencyPair
    val last: BigDecimal
    val bid: BigDecimal
    val ask: BigDecimal
    val high: BigDecimal
    val low: BigDecimal
    val vwap: BigDecimal?
    val volume: BigDecimal
}
