package com.blubber.data.market

import com.blubber.data.currency.CurrencyPair
import com.blubber.data.exchange.Exch

data class MarketDataBook(
        val orderBooks: Map<Exch, Map<CurrencyPair, OrderBook>>,
        val tickers: Map<Exch, Map<CurrencyPair, Ticker>>) {
    companion object {
        val EMPTY = MarketDataBook(emptyMap(), emptyMap())
    }
}