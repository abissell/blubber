package com.blubber.data.market

import com.blubber.data.order.LimitOrder

expect class OrderBook : MarketData {
    val asks: List<LimitOrder>
    val bids: List<LimitOrder>
}
