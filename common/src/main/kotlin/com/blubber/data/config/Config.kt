package com.blubber.data.config

@Suppress("DataClassPrivateConstructor")
data class Config private constructor(val exchSpecs: Map<Int, ExchangeSpec>) {
    companion object {
        val EMPTY = Config(emptyMap())

        private var idSource: Int = 1
    }

    fun newConfig(newExchSpec: ExchangeSpec): Config {
        if (exchSpecs.values.contains(newExchSpec)) {
            return this
        }

        return Config(exchSpecs.toMutableMap().plus(idSource++ to newExchSpec))
    }
}
