package com.blubber.data.config

import com.blubber.data.exchange.Exch
import kotlinx.serialization.Optional
import kotlinx.serialization.Serializable

@Serializable
data class ExchangeSpec(
    val exch: Exch,
    @Optional val exchangeName: String? = null,
    @Optional val exchangeDescription: String? = null,
    @Optional val userName: String? = null,
    @Optional val password: String? = null,
    @Optional val secretKey: String? = null,
    @Optional val apiKey: String? = null,
    @Optional val sslUri: String? = null,
    @Optional val plainTextUri: String? = null,
    @Optional val host: String? = null,
    @Optional val port: Int = 80,
    @Optional val proxyHost: String? = null,
    @Optional val proxyPort: Int? = null,
    @Optional val httpConnTimeout: Int = 0,
    @Optional val httpReadTimeout: Int = 0,
    @Optional val metaDataJsonFileOverride: String? = null,
    @Optional val shouldLoadRemoteMetaData: Boolean = true,
    @Optional val exchangeSpecificParameters: String? = null
) : Configuration

@Serializable
data class Foo(
    val bar: String
)

