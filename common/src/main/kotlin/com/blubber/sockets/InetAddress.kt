package com.blubber.sockets

expect class InetAddress {
    fun getHostName(): String
}

expect fun inetAddress(): InetAddress

expect fun inetAddress(hostName: String): InetAddress
