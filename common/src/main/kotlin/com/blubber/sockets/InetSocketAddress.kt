package com.blubber.sockets

expect class InetSocketAddress {
    val inetAddress: InetAddress
    val port: Int
}

expect fun inetSocketAddress(
    inetAddress: InetAddress = inetAddress(),
    port: Int = 0
): InetSocketAddress
