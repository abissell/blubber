package com.blubber.sockets

import com.blubber.data.meta.TimeUnit

interface ReadWriteSocket : AsyncSocket {
    fun getRemoteAddress(): InetSocketAddress

    suspend fun read(
        timeout: Long = 0L,
        timeUnit: TimeUnit = TimeUnit.MILLISECONDS
    ): ByteArray?

    suspend fun write(
        bytes: ByteArray,
        timeout: Long = 0L,
        timeUnit: TimeUnit = TimeUnit.MILLISECONDS
    ): Int
}
