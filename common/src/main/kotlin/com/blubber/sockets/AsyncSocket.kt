package com.blubber.sockets

import kmulti.io.Closeable

interface AsyncSocket : Closeable {
    fun getLocalAddress(): InetSocketAddress
}
