package com.blubber.platform

import kmulti.io.Closeable

interface Subscription<out T> : Closeable {
    suspend fun open(block: (T) -> Unit)
}
