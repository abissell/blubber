package com.blubber.util

fun <K, V> Map<K, V>.mergeReduce(
    other: Map<K, V>,
    reduce: (V, V) -> V
): Map<K, V> {
    val result = HashMap<K, V>(this.size + other.size)
    result.putAll(this)
    for ((key, value) in other) {
        result[key] = result[key]?.let { reduce(it, value) } ?: value
    }
    return result
}
