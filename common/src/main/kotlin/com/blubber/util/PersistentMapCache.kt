package com.blubber.util

import kotlin.jvm.Volatile

class PersistentMapCache<K, V>(initialCapacity: Int,
                               ctor: (capacity: Int) -> Map<K, V>) {

    @Volatile
    private var cache: Map<K, V> = ctor.invoke(initialCapacity)

    operator fun get(key: K): V? = cache[key]

    fun getOrPut(key: K, generator: (key: K) -> V): V {
        val existing = cache[key]
        if (existing != null) {
            return existing
        }

        putAllIfAbsent(listOf(key), generator)
        return cache[key]!! // safe since key added in putAllIfAbsent
    }

    fun putAllIfAbsent(keys: Collection<K>, generator: (key: K) -> V) {
        synchronized(this) {
            val newCache: MutableMap<K, V> = cache.toMutableMap()
            var setNew = false
            keys.forEach {
                val existing = newCache[it]
                if (existing == null) {
                    newCache[it] = generator.invoke(it)
                    setNew = true
                }
            }

            if (setNew) {
                cache = newCache.toMap()
            }
        }
    }
}
