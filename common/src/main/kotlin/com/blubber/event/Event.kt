package com.blubber.event

import com.blubber.data.config.ExchangeSpec
import com.blubber.data.exchange.Exch
import com.blubber.data.market.MarketData
import com.blubber.data.market.OrderBook
import com.blubber.data.market.Ticker
import com.blubber.data.order.LimitOrder

sealed class Event(open val head: EventHead)

data class ConfigEvent(
    override val head: EventHead,
    val config: ExchangeSpec
) : Event(head)

sealed class ExchangeEvent(
    override val head: EventHead,
    open val pullTs: Long?,
    open val exch: Exch,
    open val exchTs: Long?
) : Event(head)

data class OrderEvent(
    override val head: EventHead,
    override val pullTs: Long?,
    override val exch: Exch,
    override val exchTs: Long?,
    val order: LimitOrder,
    val replacedOrder: LimitOrder?
) : ExchangeEvent(head, pullTs, exch, exchTs)

data class FillEvent(
    override val head: EventHead,
    override val pullTs: Long?,
    override val exch: Exch,
    override val exchTs: Long?,
    val tradeId: String,
    val orderId: String
) : ExchangeEvent(head, pullTs, exch, exchTs)

sealed class MarketDataEvent<out T : MarketData>(
    override val head: EventHead,
    override val pullTs: Long?,
    override val exch: Exch,
    override val exchTs: Long?,
    open val data: T
) : ExchangeEvent(head, pullTs, exch, exchTs)

data class OrderBookEvent(
    override val head: EventHead,
    override val pullTs: Long?,
    override val exch: Exch,
    override val exchTs: Long?,
    val orderBook: OrderBook
) : MarketDataEvent<OrderBook>(head, pullTs, exch, exchTs, orderBook)

data class TickerEvent(
    override val head: EventHead,
    override val pullTs: Long?,
    override val exch: Exch,
    override val exchTs: Long?,
    val ticker: Ticker
) : MarketDataEvent<Ticker>(head, pullTs, exch, exchTs, ticker)

