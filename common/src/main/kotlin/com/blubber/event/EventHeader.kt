package com.blubber.event

data class EventHead(
    val seqNum: Long,
    val tsMillis: Long,
    val tsNanos: Long,
    val origin: EventOrigin
)
