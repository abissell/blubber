package com.blubber.event

enum class EventOrigin {
    EXCHANGE_PULL,
    EXCHANGE_PUSH,
    UI,
    INTERNAL
}
