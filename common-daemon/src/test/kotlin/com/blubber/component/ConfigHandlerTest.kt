package com.blubber.component

import com.blubber.data.config.ExchangeSpec
import com.blubber.data.exchange.Exch
import com.blubber.event.ConfigEvent
import com.blubber.event.EventHead
import com.blubber.event.EventOrigin
import com.blubber.pipeline.PipelineState
import klogging.KLoggers
import kotlin.test.Test

class ConfigHandlerTest {
    private val logger = KLoggers.logger(this::class)

    @Test
    fun testExchConfigsEvent() {
        val newExchSpec = ExchangeSpec(
            exch = Exch.POLONIEX,
            secretKey = "ABC",
            apiKey = "DEF"
        )
        val newConfigEvent = ConfigEvent(
            head = EventHead(
                seqNum = 123L,
                tsMillis = 123456L,
                tsNanos = 234567L,
                origin = EventOrigin.UI
            ),
            config = newExchSpec
        )
        configHandler(newConfigEvent, PipelineState.EMPTY)
    }
}
