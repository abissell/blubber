package com.blubber.exchange

import com.blubber.data.exchange.Exch
import com.blubber.data.config.ExchangeSpec
import kotlin.test.Ignore
import kotlin.test.Test
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class ExchangesTest {
    @Test
    // TODO: Remove network dependency created by streaming exchange init
    @Ignore
    fun testExchangeCaching() {
        val poloniexOne = Exchanges.getExchange(ExchangeSpec(
                exch = Exch.POLONIEX,
                secretKey = "123"
        ))
        assertTrue { poloniexOne.id() == 1 }
        val poloniexTwo = Exchanges.getExchange(ExchangeSpec(
                Exch.POLONIEX,
                secretKey = "456"
        ))
        assertTrue { poloniexTwo.id() == 2}
        assertFalse { poloniexOne == poloniexTwo }
        val poloniexThree = Exchanges.getExchange(ExchangeSpec(
                exch = Exch.POLONIEX,
                secretKey = "123"
        ))
        assertTrue { poloniexThree.id() == 1 }
        assertTrue { poloniexOne == poloniexThree }
        assertTrue { poloniexOne === poloniexThree }
    }
}
