package com.blubber.exchange

import com.blubber.data.config.ExchangeSpec
import com.blubber.data.exchange.Exch
import kotlin.test.Test
import kotlin.test.assertTrue

class ExchangeSpecTest {
    @Test
    fun testMinimalExchangeSpec() {
        val spec = ExchangeSpec(
                exch = Exch.POLONIEX,
                apiKey = "apiKey")
        assertTrue {
            spec == ExchangeSpec(
                    exch = Exch.POLONIEX,
                    apiKey = "apiKey")
        }
        println(spec.exch)
    }
}
