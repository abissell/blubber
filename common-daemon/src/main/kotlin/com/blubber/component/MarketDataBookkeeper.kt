package com.blubber.component

import com.blubber.event.*
import com.blubber.pipeline.PipelineOp
import com.blubber.pipeline.PipelineResult
import com.blubber.pipeline.PipelineState
import klogging.KLoggers

private val logger = KLoggers.logger("marketDataBookkeeper")

fun <T : ExchangeEvent> marketDataBookkeeper()
        : PipelineOp<T> =
    { event: ExchangeEvent, state ->
        when (event) {
            is OrderEvent -> handleOrderEvent(event, state)
            is FillEvent -> handleFillEvent(event, state)
            is OrderBookEvent -> handleOrderBookEvent(event, state)
            is TickerEvent -> handleTickerEvent(event, state)
        }
    }

private fun handleOrderEvent(event: OrderEvent, priorState: PipelineState): PipelineResult {
    logger.debug { "Got OrderEvent! $event" }
    return PipelineResult(priorState)
}

private fun handleFillEvent(event: FillEvent, priorState: PipelineState): PipelineResult {
    logger.debug { "Got FillEvent! $event" }
    return PipelineResult(priorState)
}

private fun handleOrderBookEvent(event: OrderBookEvent, priorState: PipelineState): PipelineResult {
    logger.debug { "Got OrderBookEvent! $event" }
    return PipelineResult(priorState)
}

private fun handleTickerEvent(event: TickerEvent, priorState: PipelineState): PipelineResult {
    logger.debug { "Got TickerEvent! $event" }
    return PipelineResult(priorState)
}
