package com.blubber.component

import com.blubber.event.ConfigEvent
import com.blubber.pipeline.PipelineOp
import com.blubber.pipeline.PipelineResult
import com.blubber.pipeline.PipelineState
import klogging.KLoggers

private val logger = KLoggers.logger("configHandler")

val configHandler: PipelineOp<ConfigEvent> =
    { event, state -> handleConfigEvent(event, state) }

private fun handleConfigEvent(event: ConfigEvent, priorState: PipelineState): PipelineResult {
    logger.debug { "Got ConfigEvent! $event" }
    val newConfig = priorState.config.newConfig(event.config)
    logger.debug { "After event had new config: $newConfig" }
    return PipelineResult(resultState = priorState.copy(config = newConfig))
}
