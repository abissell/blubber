package com.blubber.component

import com.blubber.event.ExchangeEvent
import com.blubber.event.FillEvent
import com.blubber.event.MarketDataEvent
import com.blubber.event.OrderEvent
import com.blubber.pipeline.PipelineOp
import com.blubber.pipeline.PipelineResult
import com.blubber.pipeline.PipelineState
import klogging.KLoggers

private val logger = KLoggers.logger("tradeActionManager")

fun <T : ExchangeEvent> tradeActionManager(): PipelineOp<T> =
    { event: ExchangeEvent, state ->
        when (event) {
            is OrderEvent -> handleOrderEvent(event, state)
            is FillEvent -> handleFillEvent(event, state)
            is MarketDataEvent<*> -> handleMarketDataEvent(event, state)
        }
    }

private fun handleOrderEvent(event: OrderEvent, priorState: PipelineState): PipelineResult {
    logger.debug { "Got OrderEvent $event!" }
    return PipelineResult(priorState)
}

private fun handleFillEvent(event: FillEvent, priorState: PipelineState): PipelineResult {
    logger.debug { "Got FillEvent $event!" }
    return PipelineResult(priorState)
}

private fun handleMarketDataEvent(
    event: MarketDataEvent<*>,
    priorState: PipelineState
): PipelineResult {
    logger.debug { "Got MarketDataEvent! $event!" }
    return PipelineResult(priorState)
}
