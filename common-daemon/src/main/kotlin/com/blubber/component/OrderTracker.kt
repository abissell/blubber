package com.blubber.component

import com.blubber.event.ExchangeEvent
import com.blubber.pipeline.PipelineOp
import com.blubber.pipeline.PipelineResult
import com.blubber.pipeline.PipelineState
import klogging.KLoggers

private val logger = KLoggers.logger("orderTracker")

fun <T : ExchangeEvent> orderTracker(): PipelineOp<T> =
    { event, state -> handleExchangeEvent(event, state) }

private fun handleExchangeEvent(event: ExchangeEvent, priorState: PipelineState): PipelineResult {
    logger.debug { "Got OrderEvent! $event" }
    return PipelineResult(priorState)
}
