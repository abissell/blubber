package com.blubber.component

import com.blubber.event.Event
import com.blubber.pipeline.PipelineOp
import com.blubber.pipeline.PipelineResult
import com.blubber.pipeline.PipelineState
import klogging.KLoggers

private val logger = KLoggers.logger("riskManager")

fun <T : Event> riskManager(): PipelineOp<T> =
    { event, state -> handleEvent(event, state) }

private fun handleEvent(event: Event, priorState: PipelineState): PipelineResult {
    logger.debug { "Got OrderEvent! $event" }
    return PipelineResult(priorState)
}
