package com.blubber.component

import com.blubber.event.ConfigEvent
import com.blubber.event.Event
import com.blubber.event.ExchangeEvent
import com.blubber.pipeline.PipelineOp
import com.blubber.pipeline.PipelineResult
import com.blubber.pipeline.PipelineState
import klogging.KLoggers

private val logger = KLoggers.logger("orderDispatcher")

fun <T : Event> orderDispatcher(): PipelineOp<T> =
    { event: Event, state ->
        when (event) {
            is ConfigEvent -> handleConfigEvent(event, state)
            is ExchangeEvent -> handleExchangeEvent(event, state)
        }
    }

private fun handleConfigEvent(event: ConfigEvent, priorState: PipelineState): PipelineResult {
    logger.debug { "Got ConfigEvent! $event" }
    return PipelineResult(priorState)
}

private fun handleExchangeEvent(event: ExchangeEvent, priorState: PipelineState): PipelineResult {
    logger.debug { "Got ExchangeEvent! $event" }
    return PipelineResult(priorState)
}
