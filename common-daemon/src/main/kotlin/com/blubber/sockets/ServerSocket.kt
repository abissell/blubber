package com.blubber.sockets

interface ServerSocket : AsyncSocket {
    suspend fun accept(): ReadWriteSocket
}
