package com.blubber.pipeline

import com.blubber.data.config.Config
import com.blubber.data.market.MarketDataBook
import com.blubber.data.order.OrderGuide
import com.blubber.data.order.OrderLedger

@Suppress("DataClassPrivateConstructor")
data class PipelineState private constructor(
    val config: Config = Config.EMPTY,
    val orderLedger: OrderLedger = OrderLedger.EMPTY,
    val marketDataBook: MarketDataBook = MarketDataBook.EMPTY,
    val orderGuide: OrderGuide = OrderGuide.EMPTY,
    val riskManagedOrderGuide: OrderGuide = OrderGuide.EMPTY
) {
    companion object {
        val EMPTY = PipelineState()
    }

    fun initialResult(): PipelineResult = PipelineResult(resultState = this)
}

