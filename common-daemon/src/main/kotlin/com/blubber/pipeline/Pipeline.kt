package com.blubber.pipeline

import com.blubber.component.*
import com.blubber.event.ConfigEvent
import com.blubber.event.Event
import com.blubber.event.ExchangeEvent

class Pipeline {
    private var state = PipelineState.EMPTY

    private val configEventComponents: List<PipelineOp<ConfigEvent>> =
        listOf(
            configHandler,
            riskManager(),
            orderDispatcher()
        )

    private val exchangeEventComponents: List<PipelineOp<ExchangeEvent>> =
        listOf(
            orderTracker(),
            marketDataBookkeeper(),
            tradeActionManager(),
            riskManager(),
            orderDispatcher()
        )

    fun process(event: Event) {
        val initialResult = state.initialResult()
        val (newState, newEvents) = when (event) {
            is ConfigEvent -> runPipeline(event, initialResult, configEventComponents)
            is ExchangeEvent -> runPipeline(event, initialResult, exchangeEventComponents)
        }
        state = newState
        // Send newEvents to priority child-event queue
    }

    private fun <T : Event> startPipeline(
        event: T,
        initialState: PipelineState,
        components: List<PipelineOp<T>>
    ): PipelineResult = runPipeline(event, initialState.initialResult(), components)

    private tailrec fun <T : Event> runPipeline(
        event: T,
        result: PipelineResult,
        components: List<PipelineOp<T>>
    ): PipelineResult =
        if (components.isEmpty()) {
            result
        } else {
            val newResult = components.first()(event, result.resultState)
            runPipeline(event, result.merge(newResult), components.drop(1))
        }
}
