package com.blubber.pipeline

typealias PipelineOp<T> = (T, PipelineState) -> PipelineResult
