package com.blubber.pipeline

import com.blubber.event.Event

data class PipelineResult(
    val resultState: PipelineState,
    val generatedEvents: List<Event> = emptyList()
) {
    fun merge(newResult: PipelineResult): PipelineResult {
        val mergedEvents = when {
            newResult.generatedEvents.isEmpty() -> generatedEvents
            else -> generatedEvents.toMutableList().apply {
                addAll(newResult.generatedEvents)
            }
        }
        return PipelineResult(newResult.resultState, mergedEvents)
    }
}
