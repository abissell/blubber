package com.blubber.exchange

import com.blubber.data.exchange.Exch
import com.blubber.data.config.ExchangeSpec
import kotlin.jvm.Volatile

object Exchanges {
    private const val IDS_PER_EXCHANGE = 4

    @Volatile
    private var exchIdSource: Int = 1

    @Volatile
    private var exchCache: Map<Exch, Set<Exchange>> =
            HashMap(Exch.values().size)

    @Volatile
    private var idCache: Map<Int, Exchange> =
            HashMap(Exch.values().size * IDS_PER_EXCHANGE)

    fun getExchange(spec: ExchangeSpec): Exchange {
        val cachedExchange = fetchCachedExchange(spec)
        if (cachedExchange != null) {
            return cachedExchange
        }

        synchronized(lock = this) {
            // Synchronized attempt to fetch Exchange once more
            val refetchedExchange = fetchCachedExchange(spec)
            if (refetchedExchange != null) {
                return refetchedExchange
            }

            val exch = spec.exch
            val newExchId = exchIdSource++
            val newExchange = Exchange(spec, newExchId)
            val newCacheForExch: MutableSet<Exchange> =
                    exchCache[exch]?.toMutableSet() ?: HashSet(IDS_PER_EXCHANGE)
            newCacheForExch.add(newExchange)

            val newExchCache = exchCache.toMutableMap()
            newExchCache[exch] = newCacheForExch
            exchCache = newExchCache.toMap()

            val newIdCache = idCache.toMutableMap()
            newIdCache[newExchId] = newExchange
            idCache = newIdCache.toMap()

            return newExchange
        }
    }

    private fun fetchCachedExchange(spec: ExchangeSpec): Exchange? =
            exchCache[spec.exch]?.find { it.spec() == spec }
}
