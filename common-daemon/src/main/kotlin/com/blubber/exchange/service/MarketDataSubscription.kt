package com.blubber.exchange.service

import com.blubber.data.market.MarketData
import com.blubber.platform.Subscription

expect class MarketDataSubscription<out T : MarketData> : Subscription<T>