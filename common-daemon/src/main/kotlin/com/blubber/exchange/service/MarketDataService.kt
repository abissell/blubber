package com.blubber.exchange.service

import com.blubber.data.currency.CurrencyPair
import com.blubber.data.market.MarketData
import com.blubber.data.market.OrderBook
import com.blubber.data.market.Ticker

expect class MarketDataService {
    fun getTicker(currencyPair: CurrencyPair, vararg args: Any): Ticker
    fun getOrderBook(currencyPair: CurrencyPair, vararg args: Any): OrderBook
    fun getStreamingOrderBookSubscription(currencyPair: CurrencyPair)
            : MarketDataSubscription<OrderBook>?
}
