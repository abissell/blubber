package com.blubber.exchange.service

import com.blubber.data.exchange.Exch
import com.blubber.util.PersistentMapCache

object MarketDataServices {
    private val exchCache = PersistentMapCache(Exch.values().size) {
        HashMap<Exch, MarketDataService>(it)
    }

    fun getMarketDataService(exch: Exch, ctor: () -> MarketDataService) =
            exchCache.getOrPut(exch) { ctor() }
}
