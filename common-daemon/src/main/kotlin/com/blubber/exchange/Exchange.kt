package com.blubber.exchange

import com.blubber.data.config.ExchangeSpec
import com.blubber.exchange.service.MarketDataService

expect class Exchange internal constructor(
        exchangeSpec: ExchangeSpec,
        exchangeId: Int) {
    fun spec(): ExchangeSpec
    fun id(): Int
    fun getMarketDataService(): MarketDataService
    fun disconnect()
}
