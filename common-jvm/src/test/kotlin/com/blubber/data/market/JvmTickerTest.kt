package com.blubber.data.market

import org.knowm.xchange.currency.CurrencyPair
import java.math.BigDecimal
import java.util.Date
import kotlin.test.assertNotNull
import kotlin.test.Test

class JvmTickerTest {
    @Test
    fun testJvmTicker() {
        val builder: org.knowm.xchange.dto.marketdata.Ticker.Builder =
                org.knowm.xchange.dto.marketdata.Ticker.Builder()
        builder.currencyPair(CurrencyPair("XMR", "BTC"))
        val bigDecimal = BigDecimal("1234.5678")
        builder.last(bigDecimal)
        builder.bid(bigDecimal)
        builder.ask(bigDecimal)
        builder.high(bigDecimal)
        builder.low(bigDecimal)
        builder.vwap(bigDecimal)
        builder.volume(bigDecimal)
        builder.timestamp(Date())
        val knowmTicker = builder.build()
        val jvmTicker = Ticker(knowmTicker)
        assertNotNull(jvmTicker)
    }
}
