package com.blubber.sockets

import java.net.SocketAddress

actual data class InetSocketAddress(
    val javaAddress: java.net.InetSocketAddress
) {
    actual val inetAddress: InetAddress = javaAddress.address
    actual val port: Int = javaAddress.port
}

// Handles default InetAddress and port via JVM
actual fun inetSocketAddress(inetAddress: InetAddress, port: Int): InetSocketAddress {
    val javaAddress = java.net.InetSocketAddress(inetAddress, port)
    return InetSocketAddress(javaAddress)
}

fun SocketAddress.asInetSocketAddress(): InetSocketAddress =
    InetSocketAddress(this as java.net.InetSocketAddress)
