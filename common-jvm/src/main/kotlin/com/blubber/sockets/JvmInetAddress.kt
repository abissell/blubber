package com.blubber.sockets

actual typealias InetAddress = java.net.InetAddress

actual fun inetAddress() = java.net.InetAddress.getLocalHost()

actual fun inetAddress(hostName: String) = java.net.InetAddress.getByName(hostName)
