package com.blubber.sockets

import com.blubber.data.meta.TimeUnit
import com.blubber.meta.toJvmTimeUnit
import kmulti.logging.companionLogger
import kotlinx.coroutines.experimental.nio.aConnect
import kotlinx.coroutines.experimental.nio.aRead
import kotlinx.coroutines.experimental.nio.aWrite
import java.net.SocketAddress
import java.nio.channels.AsynchronousSocketChannel

class JvmReadWriteSocket(
    private val remoteAddress: InetSocketAddress,
    private val localAddress: InetSocketAddress = inetSocketAddress(),
    bufferSize: Int = 8192,
    private val javaSocket: AsynchronousSocketChannel =
        AsynchronousSocketChannel.open().bind(localAddress.javaAddress)
) : ReadWriteSocket {

    companion object {
        private val logger = companionLogger {}

        fun fromJavaSocket(javaSocket: AsynchronousSocketChannel) = JvmReadWriteSocket(
            remoteAddress = javaSocket.remoteAddress.asInetSocketAddress(),
            localAddress = javaSocket.localAddress.asInetSocketAddress(),
            javaSocket = javaSocket
        )
    }

    private val readBuffer = java.nio.ByteBuffer.allocate(bufferSize)

    override fun getLocalAddress(): InetSocketAddress = localAddress

    override fun getRemoteAddress(): InetSocketAddress = remoteAddress

    override suspend fun read(timeout: Long, timeUnit: TimeUnit): ByteArray? {
        val bytes = javaSocket.aRead(readBuffer, timeout, timeUnit.toJvmTimeUnit())

        val byteArray = when {
            bytes < 0 -> null
            else -> readBuffer.array().copyOf(bytes)
        }
        readBuffer.clear()
        return byteArray
    }

    override suspend fun write(bytes: ByteArray, timeout: Long, timeUnit: TimeUnit): Int {
        val writeBuf = java.nio.ByteBuffer.wrap(bytes)
        return javaSocket.aWrite(writeBuf, timeout, timeUnit.toJvmTimeUnit())
    }

    override fun close() {
        logger.debug { "Auto-closing JvmReadWriteSocket!" }
        javaSocket.close()
    }

    suspend fun aConnect(socketAddress: SocketAddress) = javaSocket.aConnect(socketAddress)
}
