package com.blubber.meta

import com.blubber.data.meta.TimeUnit

fun TimeUnit.toJvmTimeUnit() = when (this) {
    TimeUnit.NANOSECONDS -> java.util.concurrent.TimeUnit.NANOSECONDS
    TimeUnit.MICROSECONDS -> java.util.concurrent.TimeUnit.MICROSECONDS
    TimeUnit.MILLISECONDS -> java.util.concurrent.TimeUnit.MILLISECONDS
    TimeUnit.SECONDS -> java.util.concurrent.TimeUnit.SECONDS
    TimeUnit.MINUTES -> java.util.concurrent.TimeUnit.MINUTES
    TimeUnit.HOURS -> java.util.concurrent.TimeUnit.HOURS
    TimeUnit.DAYS -> java.util.concurrent.TimeUnit.DAYS
}

