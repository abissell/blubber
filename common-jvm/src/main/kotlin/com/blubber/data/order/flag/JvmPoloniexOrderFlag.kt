package com.blubber.data.order.flag

import org.knowm.xchange.poloniex.dto.trade.PoloniexOrderFlags

fun translateFlag(flag: PoloniexOrderFlags): PoloniexOrderFlag = when (flag) {
    PoloniexOrderFlags.FILL_OR_KILL -> PoloniexOrderFlag.FILL_OR_KILL
    PoloniexOrderFlags.IMMEDIATE_OR_CANCEL -> PoloniexOrderFlag.IMMEDIATE_OR_CANCEL
    PoloniexOrderFlags.POST_ONLY -> PoloniexOrderFlag.POST_ONLY
    PoloniexOrderFlags.MARGIN -> PoloniexOrderFlag.MARGIN
}

