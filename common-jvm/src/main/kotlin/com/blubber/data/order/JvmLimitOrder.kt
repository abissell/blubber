package com.blubber.data.order

import com.blubber.data.currency.CurrencyPair
import com.blubber.data.order.flag.translateFlag
import org.knowm.xchange.dto.Order
import org.knowm.xchange.poloniex.dto.trade.PoloniexOrderFlags
import java.math.BigDecimal

actual data class LimitOrder actual constructor(
        actual val type: OrderType,
        actual val status: OrderStatus?,
        actual val originalAmount: BigDecimal,
        actual val cumulativeAmount: BigDecimal?,
        actual val averagePrice: BigDecimal?,
        actual val currencyPair: CurrencyPair,
        actual val id: String?,
        actual val ts: Long?,
        actual val limitPrice: BigDecimal,
        actual val orderFlags: Set<OrderFlag>) : Comparable<LimitOrder> {

    constructor(limitOrder: org.knowm.xchange.dto.trade.LimitOrder) : this(
            type = translateOrderType(limitOrder.type),
            status = translateOrderStatus(limitOrder.status),
            originalAmount = limitOrder.originalAmount,
            cumulativeAmount = limitOrder.cumulativeAmount,
            averagePrice = limitOrder.averagePrice,
            currencyPair = CurrencyPair.fromKnowm(limitOrder.currencyPair),
            id = limitOrder.id,
            ts = limitOrder.timestamp?.time,
            limitPrice = limitOrder.limitPrice,
            orderFlags = translateOrderFlags(limitOrder.orderFlags)
    )

    override fun compareTo(other: LimitOrder): Int = this.compare(other)
}

private fun translateOrderType(
        orderType: org.knowm.xchange.dto.Order.OrderType): OrderType =
        when (orderType) {
            Order.OrderType.BID -> OrderType.BID
            Order.OrderType.ASK -> OrderType.ASK
            Order.OrderType.EXIT_BID ->
                throw IllegalArgumentException("$orderType")
            Order.OrderType.EXIT_ASK ->
                throw IllegalArgumentException("$orderType")
        }

private fun translateOrderStatus(
        orderStatus: org.knowm.xchange.dto.Order.OrderStatus?): OrderStatus? {
    if (orderStatus == null) {
        return null
    }

    return when (orderStatus) {
        Order.OrderStatus.PENDING_NEW -> OrderStatus.PENDING_NEW
        Order.OrderStatus.NEW -> OrderStatus.NEW
        Order.OrderStatus.PARTIALLY_FILLED -> OrderStatus.PARTIALLY_FILLED
        Order.OrderStatus.FILLED -> OrderStatus.FILLED
        Order.OrderStatus.PENDING_CANCEL -> OrderStatus.PENDING_CANCEL
        Order.OrderStatus.PARTIALLY_CANCELED -> OrderStatus.PARTIALLY_CANCELED
        Order.OrderStatus.CANCELED -> OrderStatus.CANCELED
        Order.OrderStatus.PENDING_REPLACE -> OrderStatus.PENDING_REPLACE
        Order.OrderStatus.REPLACED -> OrderStatus.REPLACED
        Order.OrderStatus.STOPPED -> OrderStatus.STOPPED
        Order.OrderStatus.REJECTED -> OrderStatus.REJECTED
        Order.OrderStatus.EXPIRED -> OrderStatus.EXPIRED
        Order.OrderStatus.UNKNOWN -> OrderStatus.UNKNOWN
    }
}

private fun translateOrderFlags(orderFlags: Set<Order.IOrderFlags>):
        Set<OrderFlag> {
    return orderFlags.map {
        when (it) {
            is PoloniexOrderFlags -> translateFlag(it)
            else -> throw IllegalArgumentException("$it")
        }
    }.toSet()
}
