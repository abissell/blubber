package com.blubber.data.exchange

import info.bitrich.xchangestream.poloniex2.PoloniexStreamingExchange
import org.knowm.xchange.ExchangeSpecification
import org.knowm.xchange.binance.BinanceExchange
import org.knowm.xchange.bitfinex.v1.BitfinexExchange
import org.knowm.xchange.bitflyer.BitflyerExchange
import org.knowm.xchange.bitmex.BitmexExchange
import org.knowm.xchange.bitstamp.BitstampExchange
import org.knowm.xchange.bittrex.BittrexExchange
import org.knowm.xchange.coinmate.CoinmateExchange
import org.knowm.xchange.cryptopia.CryptopiaExchange
import org.knowm.xchange.gdax.GDAXExchange
import org.knowm.xchange.gemini.v1.GeminiExchange
import org.knowm.xchange.hitbtc.v2.HitbtcExchange
import org.knowm.xchange.kraken.KrakenExchange
import org.knowm.xchange.okcoin.OkCoinExchange
import org.knowm.xchange.wex.v3.WexExchange

fun getDefaultExchangeSpecification(exch: Exch): ExchangeSpecification =
        getKnowmExchange(exch).defaultExchangeSpecification

private fun getKnowmExchange(exch: Exch): org.knowm.xchange.Exchange =
        when (exch) {
            Exch.BINANCE -> BinanceExchange()
            Exch.BITFINEX -> BitfinexExchange()
            Exch.BITFLYER -> BitflyerExchange()
            Exch.BITMEX -> BitmexExchange()
            Exch.BITSTAMP -> BitstampExchange()
            Exch.BITTREX -> BittrexExchange()
            Exch.COINMATE -> CoinmateExchange()
            Exch.CRYPTOPIA -> CryptopiaExchange()
            Exch.OKCOIN -> OkCoinExchange()
            Exch.POLONIEX -> PoloniexStreamingExchange()
            Exch.GDAX -> GDAXExchange()
            Exch.GEMINI -> GeminiExchange()
            Exch.HITBTC -> HitbtcExchange()
            Exch.KRAKEN -> KrakenExchange()
            Exch.WEX -> WexExchange()
        }

