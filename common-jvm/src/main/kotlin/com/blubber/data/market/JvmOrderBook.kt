package com.blubber.data.market

import com.blubber.data.order.LimitOrder

actual data class OrderBook(
        override val exchTs: Long?,
        actual val asks: List<LimitOrder>,
        actual val bids: List<LimitOrder>) : MarketData {
    constructor (orderBook: org.knowm.xchange.dto.marketdata.OrderBook) : this(
            exchTs = orderBook.timeStamp?.time,
            asks = orderBook.asks.map { it -> LimitOrder(it) },
            bids = orderBook.bids.map { it -> LimitOrder(it) }
    )
}
