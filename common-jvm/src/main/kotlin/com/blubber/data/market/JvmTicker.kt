package com.blubber.data.market

import com.blubber.data.currency.CurrencyPair
import kmulti.bignumber.BigDecimal

actual data class Ticker(
    private val ticker: org.knowm.xchange.dto.marketdata.Ticker,
    override val exchTs: Long? = ticker.timestamp?.time,
    actual val currencyPair: CurrencyPair = CurrencyPair.fromKnowm(ticker.currencyPair!!),
    actual val last: BigDecimal = ticker.last!!,
    actual val bid: BigDecimal = ticker.bid!!,
    actual val ask: BigDecimal = ticker.ask!!,
    actual val high: BigDecimal = ticker.high!!,
    actual val low: BigDecimal = ticker.low!!,
    actual val vwap: BigDecimal? = ticker.vwap,
    actual val volume: BigDecimal = ticker.volume!!
) : MarketData
