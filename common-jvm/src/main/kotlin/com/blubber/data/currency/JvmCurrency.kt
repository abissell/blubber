package com.blubber.data.currency

import com.blubber.util.PersistentMapCache

@Suppress("DataClassPrivateConstructor")
actual data class Currency private actual constructor(
        actual val commonCode: String,
        actual val isoCode: String?,
        actual val name: String?,
        actual val symbol: String?,
        actual val alternativeCodes: List<String>?) : Comparable<Currency> {

    val knowm = org.knowm.xchange.currency.Currency.getInstance(commonCode)!!

    actual companion object {
        @JvmStatic
        private val cache: PersistentMapCache<String, Currency> =
                PersistentMapCache(
                        initialCapacity = 400,
                        ctor = { capacity: Int ->
                            HashMap<String, Currency>(capacity)
                        })

        init {
            // Pre-initialize cache from Currency values in knowm library
            val codes = org.knowm.xchange.currency.Currency
                    .getAvailableCurrencyCodes()
            cache.putAllIfAbsent(keys = codes) { Currency(it) }
        }

        @JvmStatic
        actual operator fun invoke(code: String): Currency =
                cache.getOrPut(code) {
                    fromKnowm(org.knowm.xchange.currency.Currency
                            .getInstance(code))
                }

        @JvmStatic
        fun fromKnowm(knowmCcy: org.knowm.xchange.currency.Currency): Currency {
            val commonCode = knowmCcy.currencyCode
            val knowmAlternativeCodes: MutableSet<String> =
                    knowmCcy.currencyCodes.toMutableSet()
            knowmAlternativeCodes.remove(commonCode)
            val alternativeCodes: List<String>? =
                    if (knowmAlternativeCodes.isEmpty()) null
                    else knowmAlternativeCodes.toList()

            return Currency(
                    commonCode = commonCode,
                    isoCode = knowmCcy.iso4217Currency?.currencyCode,
                    name = knowmCcy.displayName,
                    symbol = knowmCcy.symbol,
                    alternativeCodes = alternativeCodes)
        }
    }

    override fun compareTo(other: Currency): Int = this.compare(other)
}
