package com.blubber.data.currency

actual data class CurrencyPair actual constructor(
        actual val base: Currency,
        actual val counter: Currency) : Comparable<CurrencyPair> {

    override fun compareTo(other: CurrencyPair): Int = this.compare(other)

    fun toKnowm(): org.knowm.xchange.currency.CurrencyPair {
        return org.knowm.xchange.currency.CurrencyPair(
                base.knowm,
                counter.knowm)
    }

    companion object {
        @JvmStatic
        fun fromKnowm(knowm: org.knowm.xchange.currency.CurrencyPair) =
                CurrencyPair(
                        Currency.fromKnowm(knowm.base),
                        Currency.fromKnowm(knowm.counter)
                )
    }
}


