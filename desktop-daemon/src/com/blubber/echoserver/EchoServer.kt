package com.blubber.echoserver

import com.blubber.data.meta.TimeUnit
import com.blubber.sockets.JvmServerSocket
import com.blubber.sockets.ReadWriteSocket
import com.blubber.sockets.inetSocketAddress
import kmulti.io.use
import kmulti.logging.topLevelLogger
import kotlinx.coroutines.experimental.Job
import kotlinx.coroutines.experimental.cancelChildren
import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.runBlocking

private val logger = topLevelLogger {}

const val PORT = 12345
const val CLIENT_READ_TIMEOUT: Long = 120_000L
const val CLIENT_WRITE_TIMEOUT: Long = 120_000L

fun main(args: Array<String>) {
    runEchoServer()
}

fun runEchoServer() {
    val localAddress = inetSocketAddress(port = PORT)
    val serverSocket = JvmServerSocket(localAddress = localAddress)

    logger.debug { "Listening on localAddress $localAddress" }

    runBlocking {
        // loop and accept connections forever
        serverSocket.use { socket ->
            val jb = Job()
            launch(parent = jb) {
                while (true) {
                    logger.debug { "Starting client accept loop" }

                    val client = socket.accept()
                    val remoteAddress = client.getRemoteAddress()
                    logger.debug { "Accepted new client with remoteAddress $remoteAddress" }

                    // just start a new coroutine for each client connection
                    launch(coroutineContext, parent = jb) {
                        try {
                            val endResult = handleClient(client)
                            logger.debug {
                                "Client connection to $remoteAddress has terminated normally, " +
                                        "endResult was $endResult"
                            }
                            if (endResult == "stop") {
                                logger.debug { "Running jb.cancelChildren()" }
                                jb.cancelChildren()
                                logger.debug { "jb.cancelChildren() was successful" }
                                jb.cancel()
                                logger.debug { "jb.cancel() was successful" }
                            }
                        } catch (ex: Throwable) {
                            logger.debug {
                                "Client connection has terminated because of $ex"
                            }
                        }
                    }
                }
            }

            logger.debug { "Waiting for client accept job to finish ..." }
            jb.join()
            logger.debug { "Client accept job finished!" }
        }
    }

    logger.debug { "Exited main echo server coroutine scope" }
}

suspend fun handleClient(
    client: ReadWriteSocket
): String {
    var result = ""
    while (true) {
        val bytes = client.read(CLIENT_READ_TIMEOUT, TimeUnit.MILLISECONDS)
        bytes ?: break
        if (bytes.size < 0) break
        val bytesWritten = client.write(bytes, CLIENT_WRITE_TIMEOUT, TimeUnit.MILLISECONDS)
        logger.debug {
            "From ${client.getRemoteAddress()} read ${bytes.size} bytes " +
                    "and wrote $bytesWritten bytes"
        }
        result = String(bytes)
        logger.debug { "Result: $result" }
        if (result == "stop") {
            break
        }
    }
    return result
}
