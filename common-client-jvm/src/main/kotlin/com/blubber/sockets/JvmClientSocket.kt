package com.blubber.sockets

class JvmClientSocket(
    private val remoteAddress: InetSocketAddress,
    private val localAddress: InetSocketAddress = inetSocketAddress(),
    bufferSize: Int = 8192,
    private val jvmReadWriteSocket: JvmReadWriteSocket = JvmReadWriteSocket(
        remoteAddress, localAddress, bufferSize
    )
) : ReadWriteSocket by jvmReadWriteSocket, ClientSocket {

    override suspend fun connect(): ClientSocket {
        jvmReadWriteSocket.aConnect(remoteAddress.javaAddress)
        return this
    }
}
