package com.blubber.sockets

actual data class InetSocketAddress(actual val inetAddress: InetAddress, actual val port: Int)

actual fun inetSocketAddress(inetAddress: InetAddress, port: Int): InetSocketAddress {
    if (port == 0) throw Exception("Cannot get ephemeral ports in JS!")
    return InetSocketAddress(inetAddress, port)
}
