package com.blubber.sockets

actual class InetAddress(private val hostName: String) {
    actual fun getHostName(): String = hostName
}

actual fun inetAddress() = inetAddress("localhost")

actual fun inetAddress(hostName: String): InetAddress = InetAddress(hostName)
