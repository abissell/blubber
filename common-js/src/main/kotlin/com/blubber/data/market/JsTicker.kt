package com.blubber.data.market

import com.blubber.data.currency.CurrencyPair
import kmulti.bignumber.BigDecimal

actual data class Ticker(
    override val exchTs: Long?,
    actual val currencyPair: CurrencyPair,
    actual val last: BigDecimal,
    actual val bid: BigDecimal,
    actual val ask: BigDecimal,
    actual val high: BigDecimal,
    actual val low: BigDecimal,
    actual val vwap: BigDecimal?,
    actual val volume: BigDecimal
) : MarketData
