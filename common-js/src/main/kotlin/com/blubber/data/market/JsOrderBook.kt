package com.blubber.data.market

import com.blubber.data.order.LimitOrder

actual data class OrderBook(
        override val exchTs: Long?,
        actual val asks: List<LimitOrder>,
        actual val bids: List<LimitOrder>) : MarketData
