package com.blubber.data.order

import com.blubber.data.currency.CurrencyPair
import kmulti.bignumber.BigDecimal

actual data class LimitOrder actual constructor(
    actual val type: OrderType,
    actual val status: OrderStatus?,
    actual val originalAmount: BigDecimal,
    actual val cumulativeAmount: BigDecimal?,
    actual val averagePrice: BigDecimal?,
    actual val currencyPair: CurrencyPair,
    actual val id: String?,
    actual val ts: Long?,
    actual val limitPrice: BigDecimal,
    actual val orderFlags: Set<OrderFlag>
) : Comparable<LimitOrder> {

    override fun compareTo(other: LimitOrder): Int = this.compare(other)
}
