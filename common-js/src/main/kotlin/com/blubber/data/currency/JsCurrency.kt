package com.blubber.data.currency

@Suppress("DataClassPrivateConstructor")
actual data class Currency private actual constructor(
        actual val commonCode: String,
        actual val isoCode: String?,
        actual val name: String?,
        actual val symbol: String?,
        actual val alternativeCodes: List<String>?) : Comparable<Currency> {

    actual companion object {
        actual operator fun invoke(code: String): Currency =
                Currency(commonCode = code,
                         isoCode = "blah",
                         name = "blah",
                         symbol = "blah",
                         alternativeCodes = emptyList())
    }

    override fun compareTo(other: Currency): Int = this.compare(other)
}
