package com.blubber.data.currency

actual data class CurrencyPair actual constructor(
        actual val base: Currency,
        actual val counter: Currency) : Comparable<CurrencyPair> {

    override fun compareTo(other: CurrencyPair): Int = this.compare(other)
}


