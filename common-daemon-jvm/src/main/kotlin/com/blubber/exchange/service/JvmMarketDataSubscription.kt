package com.blubber.exchange.service

import com.blubber.data.market.MarketData
import com.blubber.platform.Subscription
import io.reactivex.Observable
import klogging.KLoggers
import kotlinx.coroutines.experimental.channels.SubscriptionReceiveChannel
import kotlinx.coroutines.experimental.rx2.openSubscription

actual class MarketDataSubscription<out T : MarketData>(
        private val sub: Observable<T>) : Subscription<T> {

    private val logger = KLoggers.logger(this::class)

    private val channel: SubscriptionReceiveChannel<T> by lazy {
        sub.openSubscription()
    }

    override suspend fun open(block: (T) -> Unit) {
        channel.use {
            while (!channel.isClosedForReceive) {
                var counter = 0
                var coalesced: T? = null
                while (!channel.isEmpty) {
                    counter++
                    coalesced = channel.receiveOrNull()
                }
                coalesced?.let {
                    logger.debug { "Coalesced $counter values into update" }
                    block.invoke(it)
                }
            }
        }
    }

    override fun close() {
        channel.close()
    }
}
