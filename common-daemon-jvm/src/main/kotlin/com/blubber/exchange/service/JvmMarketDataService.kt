package com.blubber.exchange.service

import com.blubber.data.currency.CurrencyPair
import com.blubber.data.market.OrderBook
import com.blubber.data.market.Ticker
import info.bitrich.xchangestream.core.StreamingMarketDataService

actual class MarketDataService internal constructor(
        private val svc:
        org.knowm.xchange.service.marketdata.MarketDataService,
        private val streamingSvc: StreamingMarketDataService?) {

    actual fun getTicker(currencyPair: CurrencyPair,
                         vararg args: Any): Ticker =
            Ticker(svc.getTicker(currencyPair.toKnowm(), *args))

    actual fun getOrderBook(currencyPair: CurrencyPair,
                            vararg args: Any): OrderBook =
            OrderBook(svc.getOrderBook(currencyPair.toKnowm(), *args))

    actual fun getStreamingOrderBookSubscription(currencyPair: CurrencyPair)
            : MarketDataSubscription<OrderBook>? {
        if (streamingSvc == null) {
            return null
        }

        val observable = streamingSvc.getOrderBook(currencyPair.toKnowm())
                .map { OrderBook(it) }
        return MarketDataSubscription(observable)
    }
}
