package com.blubber.exchange

import com.blubber.data.config.ExchangeSpec
import com.blubber.data.exchange.getDefaultExchangeSpecification
import org.knowm.xchange.ExchangeSpecification

internal fun toKnowmSpec(spec: ExchangeSpec): ExchangeSpecification {
    val exch = spec.exch
    val knowmSpec = getDefaultExchangeSpecification(exch)

    spec.secretKey?.let { knowmSpec.secretKey = it }
    spec.apiKey?.let { knowmSpec.apiKey = it }
    spec.sslUri?.let { knowmSpec.sslUri = it }
    spec.plainTextUri?.let { knowmSpec.plainTextUri = it }
    spec.host?.let { knowmSpec.host = it }
    knowmSpec.port = spec.port
    spec.proxyHost?.let { knowmSpec.proxyHost = it }
    spec.proxyPort?.let { knowmSpec.proxyPort = it }

    return knowmSpec
}
