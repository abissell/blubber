package com.blubber.exchange

import com.blubber.data.config.ExchangeSpec
import com.blubber.exchange.service.MarketDataService
import com.blubber.exchange.service.MarketDataServices
import info.bitrich.xchangestream.core.StreamingExchange
import info.bitrich.xchangestream.core.StreamingExchangeFactory
import klogging.KLoggers
import org.knowm.xchange.ExchangeFactory
import org.knowm.xchange.ExchangeSpecification

actual data class Exchange internal actual constructor(
        private val exchangeSpec: ExchangeSpec,
        private val exchangeId: Int) {

    private val logger = KLoggers.logger(this::class)

    private val exch = exchangeSpec.exch

    private val knowmSpec = toKnowmSpec(exchangeSpec)

    private val knowmExchange =
            ExchangeFactory.INSTANCE.createExchange(knowmSpec)
    private val streamingExchange: StreamingExchange? =
            createStreamingExchange(knowmSpec)

    private val marketDataService: MarketDataService =
            MarketDataServices.getMarketDataService(exch) {
                val streamingExchange = createStreamingExchange(knowmSpec)
                MarketDataService(
                        knowmExchange.marketDataService,
                        streamingExchange?.streamingMarketDataService)
            }

    actual fun spec(): ExchangeSpec = exchangeSpec
    actual fun id(): Int = exchangeId
    actual fun getMarketDataService(): MarketDataService = marketDataService

    private fun createStreamingExchange(knowmSpec: ExchangeSpecification)
            : StreamingExchange? {
        logger.debug {
            "Attempting to create streaming exchange from " +
                    "knowmSpec $knowmSpec"
        }
        return try {
            val streamingExchange =
                    StreamingExchangeFactory.INSTANCE.createExchange(knowmSpec)
            streamingExchange.connect().blockingAwait()
            logger.debug { "Successfully created streaming exchange!" }
            return streamingExchange
        } catch (e: Exception) {
            logger.debug { e }
            null
        }
    }

    actual fun disconnect() {
        logger.debug { "Disconnecting from $exch" }
        streamingExchange?.disconnect()?.subscribe {
            logger.debug { "Disconnected from $exch" }
        }
    }
}
