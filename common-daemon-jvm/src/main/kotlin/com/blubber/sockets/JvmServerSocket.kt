package com.blubber.sockets

import kmulti.logging.CompanionLogger
import kotlinx.coroutines.experimental.nio.aAccept
import java.nio.channels.AsynchronousServerSocketChannel

class JvmServerSocket(
    private val localAddress: InetSocketAddress = inetSocketAddress()
) : ServerSocket {

    companion object : CompanionLogger()

    private val javaServerSocket =
        AsynchronousServerSocketChannel.open().bind(localAddress.javaAddress)

    override fun getLocalAddress(): InetSocketAddress = localAddress

    override suspend fun accept(): ReadWriteSocket {
        val client = javaServerSocket.aAccept()
        return JvmReadWriteSocket.fromJavaSocket(client)
    }

    override fun close() {
        logger.debug { "Auto-closing JvmServerSocket!" }
        javaServerSocket.close()
    }
}

