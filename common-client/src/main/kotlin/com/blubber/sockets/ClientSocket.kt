package com.blubber.sockets

interface ClientSocket : ReadWriteSocket {
    suspend fun connect(): ClientSocket
}
