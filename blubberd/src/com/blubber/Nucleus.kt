package com.blubber

import com.blubber.data.currency.Currency
import com.blubber.data.currency.CurrencyPair
import com.blubber.data.exchange.Exch
import com.blubber.data.config.ExchangeSpec
import com.blubber.exchange.Exchanges
import kmulti.logging.topLevelLogger
import kotlinx.coroutines.experimental.*
import java.util.*


private val logger = topLevelLogger {}
fun main(args: Array<String>) = runBlocking {
    TimeZone.setDefault(TimeZone.getTimeZone("UTC"))
    logger.debug { "hello from Kotlin logging" }

    val poloniex = Exchanges.getExchange(ExchangeSpec(
            Exch.POLONIEX
    ))
    val marketDataService = poloniex.getMarketDataService()

    val btcTicker = marketDataService.getTicker(
            CurrencyPair(
                    Currency("BTC"),
                    Currency("USDT")))
    logger.debug { "BTC_USDT: $btcTicker" }
    val btcOrderBook = marketDataService.getOrderBook(
            CurrencyPair(
                    Currency("BTC"),
                    Currency("USDT")),
            5)
    logger.debug { btcOrderBook }
    logger.debug { "Waiting to begin timestamp coroutine ..." }

    delay(2000L)
    val job = launch {
        repeat(1000) {
            logger.debug { "timestamp = ${System.currentTimeMillis()}" }
            delay(50L)
        }
    }
    delay(2_000L)
    job.cancelAndJoin()

    logger.debug { "Finished timestamp printing coroutine." }
    logger.debug { "Trying order book subscription in XMR_BTC" }

    val orderBookSub = marketDataService.getStreamingOrderBookSubscription(
            CurrencyPair(
                    Currency("XMR"),
                    Currency("BTC")
            )
    )

    var counter = 1
    val printMarketDataJob: Job
    try {
        printMarketDataJob = launch {
            orderBookSub?.open {
                logger.debug { "ORDER BOOK ${counter++}" }
            }
        }
        delay(5_000L)
    } finally {
        logger.debug { "!!!!!!!! Unsubscribing orderBookSub !!!!!!!" }
        orderBookSub?.close()
    }

    delay(2_000L)
    logger.debug { "Running cancelAndJoin on printMarketDataJob" }
    printMarketDataJob.cancelAndJoin()
    logger.debug { "Done!" }
    poloniex.disconnect()
    System.exit(0)
}
